/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.vnpay.contants;

/**
 *
 * @author hungnv1
 */
public enum ResponseCode {

    SUCCESS("00") {
        @Override
        public String description() {
            return "Giao dich thanh cong.";
        }
    },
    CAN_NOT_FIND_TRANSACTION("01") {
        @Override
        public String description() {
            return "Khong tim thay giao dich.";
        }
    },
    DATE_NOT_IN_FORMAT("02") {
        @Override
        public String description() {
            return "PayDate không đúng định dạng.";
        }
    },
    TXNID_IS_NULL("03") {
        @Override
        public String description() {
            return "TxnId không được null hoặc empty.";
        }
    },
    IP_DENIED("14") {
        @Override
        public String description() {
            return "IP is denied.";
        }
    },
    NOT_IN_FORMAT("11") {
        @Override
        public String description() {
            return "Format data is wrong.";
        }
    },
    NOT_FOUND_DATA("12") {
        @Override
        public String description() {
            return "Transaction not found.";
        }
    },
    MAINTENANCE("96") {
        @Override
        public String description() {
            return "System is maintaing.";
        }
    },
    EXCEPTION("99") {
        @Override
        public String description() {
            return "Internal error";
        }
    };
    private final String i;

    ResponseCode(String i) {
        this.i = i;
    }

    public String code() {
        return i;
    }

    public abstract String description();

}
