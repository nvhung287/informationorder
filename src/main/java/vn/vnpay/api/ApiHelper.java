/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.vnpay.api;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import vn.vnpay.bean.RequestMerchant;
import vn.vnpay.config.ServiceConfig;
import vn.vnpay.utils.Utils;

/**
 *
 * @author hungnv1
 */
public class ApiHelper {

    private static final Logger LOGGER = LogManager.getLogger(ApiHelper.class);

    public boolean validRequest(RequestMerchant request, String token) {
        if (null == request) {
            return false;
        } else {
            String checkSum = Utils.makeChecksum(request, ServiceConfig.getAccessKey(), token);
            return checkSum.equalsIgnoreCase(request.getChecksum());
        }
    }
}
