/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.vnpay.api;

import com.google.gson.Gson;
import java.util.UUID;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import vn.vnpay.bean.Data;
import vn.vnpay.bean.RequestMerchant;
import vn.vnpay.bean.Response;

/**
 *
 * @author hungnv1
 */
@Path("/api")
public class OrderInformationApi extends ApiHelper {

    private static final Logger LOGGER = LogManager.getLogger(ApiHelper.class);
    private final Gson gson = new Gson();
// data request example
// {"code":"00","message":"Tru tien thanh cong","msgType":"1","txnId":"VP-918875-0063-446","qrTrace":"000098634","bankCode":"IVB","mobile":"0982952390","accountNo":"","amount":"29000","payDate":"20180809094045","masterMerCode":"908405","merchantCode":"0101778163","terminalId":"FPTTEL03","addData":[{"merchantType":"4814","serviceCode":"06","masterMerCode":"908405","merchantCode":"0101778163","terminalId":"FPTTEL03","productId":"","amount":"29000","ccy":"704","qty":"1","note":""}],"checksum":"EBA026B0AEC1410E9BFECCF294C4B23B","ccy":"704","secretKey":"VNPAY"}

    @POST
    @Path("/orderInformationApi")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String orderInformationApi(String data) {
        String token = UUID.randomUUID().toString().replace("-", "");
        LOGGER.info("Token[{}] - Begin retrieve from VNPAY with data: {}", token, data);
        RequestMerchant request = gson.fromJson(data, RequestMerchant.class);
        String res;
        if (validRequest(request, token)) {
            //xử lý nội bộ merchant
            res = gson.toJson(Response.builder().code("00").message("Success").data(Data.builder().txnId(request.getTxnId()).build()).build());
        } else {
            // xử lý lỗi và trả về mã lỗi cho VNPAY
            res = gson.toJson(Response.builder().code("09").message("Qr Hết hạn").data(Data.builder().txnId(request.getTxnId()).build()).build());
        }
        LOGGER.info("Token[{}] - Response to VNPAY with data: {}", token, data);
        return res;
    }
}
