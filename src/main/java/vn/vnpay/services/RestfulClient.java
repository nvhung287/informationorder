/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.vnpay.services;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import javax.ws.rs.core.MediaType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author hungnv1
 */
public class RestfulClient {

    private static final Logger LOGGER = LogManager.getLogger(RestfulClient.class);

    private static final class SingletonHolder {

        private static final RestfulClient INSTANCE = new RestfulClient();
    }

    public static RestfulClient getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private RestfulClient() {

    }

    /**
     * this is function postDataViaHttp
     *
     * @param inputData
     * @param urlService
     * @return
     */
    public static String postDataViaHttp(final String inputData, final String urlService) {
        try {
            LOGGER.info("Post data: {} into webservice: {}.", inputData, urlService);
            URL url = new URL(urlService);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setConnectTimeout(60000);
            conn.setReadTimeout(60000);
            conn.setRequestProperty("Content-Type", "text/plain");
            conn.connect();
            StringBuilder dataInput = new StringBuilder();
            dataInput.append(inputData);
            OutputStream os = conn.getOutputStream();
            os.write(dataInput.toString().getBytes("UTF-8"));
            os.flush();
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

            StringBuilder builder = new StringBuilder();
            String output;
            while ((output = br.readLine()) != null) {
                builder.append(output);
            }
            String reponse = builder.toString();
            return reponse;
        } catch (MalformedURLException e) {
            LOGGER.error("MalformedURLException : {}.", e);
            return "";

        } catch (java.net.SocketTimeoutException e) {
            LOGGER.error("SocketTimeoutException : {}.", e);
            return "";
        } catch (IOException e) {
            LOGGER.error("IOException : {}.", e);
            return "";
        }
    }

    /**
     *
     * @param resource
     * @return
     */
    public static String getData(final String resource) {

        try {
            Client client = Client.create();
            WebResource webResource = client
                      .resource(resource);
            ClientResponse response = webResource.accept("text/plain")
                      .get(ClientResponse.class);
            String output = response.getEntity(String.class);
            return output;
        } catch (UniformInterfaceException e) {
            LOGGER.error("Post data failed by ex: {}.", e);
            return "";
        } catch (ClientHandlerException e) {
            LOGGER.error("Post data failed by ex: {}.", e);
            return "";
        }
    }

}
